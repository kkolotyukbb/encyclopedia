package edu.washington.gs.maccoss.encyclopedia.datastructures;

import java.util.Arrays;

import edu.washington.gs.maccoss.encyclopedia.algorithms.pecan.PecanLibraryEntry;
import edu.washington.gs.maccoss.encyclopedia.algorithms.pecan.PecanOneFragmentationModel;
import edu.washington.gs.maccoss.encyclopedia.algorithms.pecan.PecanSearchParameters;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.DigestionEnzyme;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.FragmentIon;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.FragmentationType;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.Ion;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.MassTolerance;
import edu.washington.gs.maccoss.encyclopedia.utils.massspec.PeptideUtils;
import gnu.trove.map.hash.TDoubleIntHashMap;
import junit.framework.TestCase;

public class FragmentationModelTest extends TestCase {
	private static final SearchParameters PARAMETERS=new PecanSearchParameters(new AminoAcidConstants(), FragmentationType.CID, new MassTolerance(50), new MassTolerance(50),
			DigestionEnzyme.getEnzyme("trypsin"), false, true, false);
	
	public void testModifiedFragmentation() {
		String sequence="A[+42.0]QRHS[+79.96633]DSSLEEK";
		FragmentationModel model=PeptideUtils.getPeptideModel(sequence, PARAMETERS.getAAConstants());
		Ion[] ions=model.getPrimaryIonObjects(FragmentationType.CID, (byte)3, false);
		for (int i=0; i<ions.length; i++) {
			System.out.println(ions[i]+"\t"+ions[i].getMass());
		}
	}
	
	public void testGetModifiedSequence() {
		String sequence="PEPT[+80]IDER";
		FragmentationModel model=PeptideUtils.getPeptideModel(sequence, PARAMETERS.getAAConstants());

		assertEquals("PEPT[+79.966331]IDER", model.getPeptideModSeq());

		sequence="[-17]EPTIDER";
		model=PeptideUtils.getPeptideModel(sequence, PARAMETERS.getAAConstants());

		assertEquals("E[-17.0]PTIDER", model.getPeptideModSeq());

		sequence="[+42]S[+80]PEPTIDER";
		model=PeptideUtils.getPeptideModel(sequence, PARAMETERS.getAAConstants());

		// FIXME this is bad! should be fixed at some point (if we start seeing weird modification structures)
		assertEquals("S[+42.010565][+79.966331]PEPTIDER", model.getPeptideModSeq());
	}

	public void testPrimaryIons() {
		String sequence="PEPT[+80]IDER";
		FragmentationModel model=PeptideUtils.getPeptideModel(sequence, PARAMETERS.getAAConstants());
		double[] ions=model.getPrimaryIons(FragmentationType.CID, (byte)2, false);
		double[] expected=new double[] {98.06063, 175.11955, 227.10323, 304.16215, 324.15603, 407.226834, 419.18915, 505.20373, 520.310934, 532.27325, 615.344054, 618.28783, 635.337934,
				712.3968540000001, 713.32095, 733.31483, 764.380534, 810.3737500000001, 841.4394540000001, 862.35743, 920.481634, 938.4922540000001, 939.4163500000001, 1018.45853, 1036.4691500000001};
		for (int i=0; i<ions.length; i++) {
			assertEquals(expected[i], ions[i], 0.1);
		}

		ions=model.getPrimaryIons(FragmentationType.ETD, (byte)2, false);
		expected=new double[] {115.08717911000001, 158.09300089, 159.10082593, 244.12977911, 287.13560089, 288.14342593, 341.18257911, 402.16260088999996, 403.17042592999996, 424.25338311,
				515.2467008899999, 516.25452593, 522.23027911, 537.33748311, 598.31750489, 599.3253299300001, 635.31437911, 652.36448311, 695.37030489, 696.29440089, 696.3781299300001,
				697.3022259300001, 750.34137911, 781.40708311, 793.3472008900001, 794.3550259300001, 824.41290489, 825.4207299300001, 879.38397911, 921.4657048900001, 922.3898008900001,
				922.4735299300002, 923.3976259300001, 937.50818311, 1019.4426008900001, 1020.4504259300002, 1035.48507911};
		for (int i=0; i<ions.length; i++) {
			assertEquals(expected[i], ions[i], 0.1);
		}
	}

	public void testFragmentation() {
		String sequence="PEPTIDER";
		double[] expectedB=new double[] {98.06063, 227.10323, 324.15599, 425.20367, 538.28773, 653.31467, 782.35727, 938.45838};
		double[] expectedY=new double[] {175.11955, 304.16214, 419.18908, 532.27314, 633.32082, 730.37359, 859.41618, 956.46894};

		FragmentationModel model=PeptideUtils.getPeptideModel(sequence, PARAMETERS.getAAConstants());
		double[] bs=FragmentIon.getMasses(model.getBIons());
		for (int i=0; i<bs.length; i++) {
			assertEquals(expectedB[i], bs[i], 0.1);
		}
		double[] ys=FragmentIon.getMasses(model.getYIons());
		for (int i=0; i<ys.length; i++) {
			assertEquals(expectedY[i], ys[i], 0.1);
		}
	}

	public void testNeutralLossFragmentation() {
		String sequence="PEPT[+80]IDER";
		double[] expectedB=new double[] {98.06063, 227.10323, 324.15599, 425.20367+80, 538.28773+80, 653.31467+80, 782.35727+80, 938.45838+80, 425.20367+80-97.976896, 538.28773+80-97.976896,
				653.31467+80-97.976896, 782.35727+80-97.976896, 938.45838+80-97.976896};
		Arrays.sort(expectedB);
		double[] expectedY=new double[] {175.11955, 304.16214, 419.18908, 532.27314, 633.32082+80, 730.37359+80, 859.41618+80, 956.46894+80, 633.32082+80-97.976896, 730.37359+80-97.976896,
				859.41618+80-97.976896, 956.46894+80-97.976896};
		Arrays.sort(expectedY);

		FragmentationModel model=PeptideUtils.getPeptideModel(sequence, PARAMETERS.getAAConstants());
		double[] bs=FragmentIon.getMasses(model.getBIons());
		for (int i=0; i<bs.length; i++) {
			assertEquals(expectedB[i], bs[i], 0.1);
		}
		double[] ys=FragmentIon.getMasses(model.getYIons());
		for (int i=0; i<ys.length; i++) {
			assertEquals(expectedY[i], ys[i], 0.1);
		}
	}

	public void testPecanLibraryEntry() {
		PecanLibraryEntry entry=getPecanEntry();
		assertEquals(4.2574906f, entry.getEuclidianDistance(), 0.1f);

		float[] expectedIntensities=new float[] {0.01644819f, 0.0056204866f, 0.084794275f, 0.091038816f, 0.30905282f, 0.14498773f, 0.35056737f, 0.28298813f, 0.34541196f, 0.37883893f, 0.39810196f,
				0.27311644f, 0.20603521f, 0.21352741f, 0.16540855f, 0.18790412f, 0.09470973f, 0.095479734f};
		float[] intensities=entry.getIntensityArray();
		for (int i=0; i<intensities.length; i++) {
			assertEquals(expectedIntensities[i], intensities[i], expectedIntensities[i]/20f);
		}
	}

	public static PecanLibraryEntry getPecanEntry() {
		TDoubleIntHashMap map=new TDoubleIntHashMap();
		map.put(114.091340467, 1428);
		map.put(147.11280417, 4179);
		map.put(227.175404467, 277);
		map.put(244.16556817, 258);
		map.put(355.233982467, 11);
		map.put(355.23398246700003, 65);
		map.put(359.19251046700003, 4);
		map.put(359.192510467, 4);
		map.put(359.1925104669999, 4);
		map.put(359.174753467, 6);
		map.put(359.17475346699996, 2);
		map.put(359.174752467, 10);
		map.put(359.17475246699996, 3);
		map.put(359.19251117, 36);
		map.put(359.192511467, 7);
		map.put(359.19251146700003, 12);
		map.put(359.19251246700003, 2);
		map.put(359.20374416999994, 35);
		map.put(359.20374417000005, 37);
		map.put(458.260924467, 8);
		map.put(458.26092446699994, 1);
		map.put(458.25102917000004, 3);
		map.put(458.25102917, 3);
		map.put(458.24316746700003, 12);
		map.put(458.243167467, 1);
		map.put(458.24316646700004, 2);
		map.put(458.243166467, 1);
		map.put(458.2397964670001, 1);
		map.put(458.239796467, 3);
		map.put(458.239795467, 4);
		map.put(458.26092517, 5);
		map.put(458.26092517000006, 3);
		map.put(458.260925467, 6);
		map.put(458.26092546700005, 1);
		map.put(458.26092617000006, 2);
		map.put(458.27215816999995, 1);
		map.put(458.27215817, 6);
		map.put(458.27215817000007, 4);
		map.put(484.276575467, 3);
		map.put(484.27657546699993, 2);
		map.put(484.27657446700005, 1);
		map.put(484.276574467, 1);
		map.put(484.27657446699993, 2);
		map.put(484.26265717, 1);
		map.put(484.258817467, 2);
		map.put(484.25544646700007, 1);
		map.put(484.255445467, 2);
		map.put(484.27657546700004, 45);
		map.put(484.27657646700004, 2);
		map.put(484.28780816999995, 10);
		map.put(484.28780817, 3);
		map.put(484.287808467, 2);
		map.put(484.28780917, 1);
		map.put(484.28780917000006, 3);
		map.put(484.28780946700005, 1);
		map.put(484.291831467, 1);
		map.put(515.282388467, 5);
		map.put(515.2823884669999, 1);
		map.put(515.28238817, 4);
		map.put(515.282387467, 1);
		map.put(515.2823874669999, 2);
		map.put(515.27249317, 1);
		map.put(515.2646314670001, 1);
		map.put(515.264631467, 2);
		map.put(515.2646304670001, 1);
		map.put(515.264630467, 3);
		map.put(515.261260467, 2);
		map.put(515.2612594670001, 2);
		map.put(515.261259467, 2);
		map.put(515.261251467, 1);
		map.put(515.25723717, 8);
		map.put(515.2572371699999, 4);
		map.put(515.2572361699999, 4);
		map.put(515.28238917, 6);
		map.put(515.282389467, 4);
		map.put(515.2823904669999, 1);
		map.put(515.282390467, 2);
		map.put(515.2936214670001, 1);
		map.put(515.2936221699999, 6);
		map.put(515.29362217, 2);
		map.put(515.2976454669999, 1);
		map.put(515.3048551700001, 1);
		map.put(541.298039467, 11);
		map.put(541.29803917, 4);
		map.put(541.298038467, 11);
		map.put(541.28814317, 1);
		map.put(541.28412117, 5);
		map.put(541.2841201699999, 1);
		map.put(541.280280467, 1);
		map.put(541.272888467, 1);
		map.put(541.272887467, 1);
		map.put(541.27288717, 3);
		map.put(541.2980394670001, 10);
		map.put(541.298040467, 1);
		map.put(541.2980404670001, 1);
		map.put(541.3092714669999, 1);
		map.put(541.30927217, 3);
		map.put(541.309272467, 1);
		map.put(541.30927317, 1);
		map.put(541.3092731700001, 1);
		map.put(541.313295467, 3);
		map.put(541.316666467, 1);
		map.put(640.3664534669999, 8);
		map.put(640.36645317, 2);
		map.put(640.366452467, 1);
		map.put(640.356556467, 3);
		map.put(640.35253517, 4);
		map.put(640.3525344669999, 1);
		map.put(640.35253417, 7);
		map.put(640.3525341699999, 3);
		map.put(640.345324467, 1);
		map.put(640.345323467, 3);
		map.put(640.3413014670001, 1);
		map.put(640.341301467, 1);
		map.put(640.3413011700001, 3);
		map.put(640.34130117, 3);
		map.put(640.341300467, 1);
		map.put(640.366453467, 14);
		map.put(640.3776871700001, 1);
		map.put(640.381700467, 1);
		map.put(640.3817094670001, 1);
		map.put(644.324981467, 2);
		map.put(644.3249814669999, 1);
		map.put(644.32498117, 1);
		map.put(644.324980467, 1);
		map.put(644.3249804669999, 1);
		map.put(644.322480467, 1);
		map.put(644.322479467, 1);
		map.put(644.322471467, 1);
		map.put(644.3184581700001, 1);
		map.put(644.31845817, 1);
		map.put(644.307858467, 1);
		map.put(644.3072244670001, 2);
		map.put(644.307224467, 5);
		map.put(644.307223467, 3);
		map.put(644.3038534670001, 1);
		map.put(644.3038524670001, 2);
		map.put(644.303852467, 3);
		map.put(644.303845467, 1);
		map.put(644.303844467, 1);
		map.put(644.2998301700001, 2);
		map.put(644.29983017, 1);
		map.put(644.299828467, 3);
		map.put(644.32498217, 5);
		map.put(644.3249824669999, 4);
		map.put(644.324982467, 3);
		map.put(644.325851467, 1);
		map.put(644.33621517, 6);
		map.put(644.3362151700001, 1);
		map.put(644.3362161700001, 3);
		map.put(644.340230467, 1);
		map.put(644.3436084670001, 1);
		map.put(644.343609467, 4);
		map.put(644.34361017, 1);
		map.put(644.343610467, 2);
		map.put(644.3436104670001, 3);
		map.put(644.34744817, 1);
		map.put(644.34744917, 4);
		map.put(644.351471467, 2);
		map.put(644.35147217, 1);
		map.put(644.3514721700001, 1);
		map.put(644.351472467, 1);
		map.put(644.35484317, 2);
		map.put(644.3548431700001, 1);
		map.put(644.355494467, 2);
		map.put(755.3933964669999, 3);
		map.put(755.3933961700001, 1);
		map.put(755.39339617, 1);
		map.put(755.393395467, 5);
		map.put(755.39338817, 1);
		map.put(755.383501467, 1);
		map.put(755.383500467, 1);
		map.put(755.3835001699999, 3);
		map.put(755.3834994669999, 2);
		map.put(755.379651467, 1);
		map.put(755.3796434669999, 1);
		map.put(755.37947717, 5);
		map.put(755.37947617, 1);
		map.put(755.375638467, 2);
		map.put(755.375637467, 1);
		map.put(755.37562917, 1);
		map.put(755.37226717, 1);
		map.put(755.372266467, 1);
		map.put(755.3722664669999, 2);
		map.put(755.37225717, 1);
		map.put(755.3722571699999, 2);
		map.put(755.3682454670001, 2);
		map.put(755.36824417, 1);
		map.put(755.3682434670001, 1);
		map.put(755.368243467, 1);
		map.put(755.36824317, 1);
		map.put(755.3583484669999, 1);
		map.put(755.35834817, 1);
		map.put(755.3578801699999, 1);
		map.put(755.357011467, 1);
		map.put(755.3570104670001, 1);
		map.put(755.357010467, 5);
		map.put(755.35700217, 3);
		map.put(755.393396467, 6);
		map.put(755.3933964670001, 3);
		map.put(755.39473317, 1);
		map.put(755.404628467, 1);
		map.put(755.4046291699999, 1);
		map.put(755.404629467, 2);
		map.put(755.4046301699999, 1);
		map.put(755.4046304669998, 1);
		map.put(755.404630467, 1);
		map.put(755.40463117, 1);
		map.put(755.404631467, 1);
		map.put(755.408643467, 2);
		map.put(755.40864417, 2);
		map.put(755.4086511699999, 1);
		map.put(755.4086514669999, 1);
		map.put(755.408652467, 2);
		map.put(755.412022467, 2);
		map.put(755.412023467, 2);
		map.put(755.412024467, 1);
		map.put(755.4158621700001, 1);
		map.put(755.4158631700001, 2);
		map.put(755.4158641700001, 1);
		map.put(755.4198851699999, 1);
		map.put(755.4198861699999, 1);
		map.put(755.4232561700001, 1);
		map.put(755.429780467, 4);
		map.put(755.4297814669999, 2);
		map.put(755.429781467, 8);
		map.put(755.429782467, 3);
		map.put(755.4297824670001, 2);
		map.put(772.3835594669999, 1);
		map.put(772.38355917, 2);
		map.put(772.383558467, 1);
		map.put(772.3810494669999, 1);
		map.put(772.37703517, 3);
		map.put(772.3770351699999, 1);
		map.put(772.37703417, 1);
		map.put(772.3736634669999, 1);
		map.put(772.3736554669999, 1);
		map.put(772.373195467, 1);
		map.put(772.372326467, 1);
		map.put(772.372325467, 1);
		map.put(772.3723254669999, 1);
		map.put(772.36980717, 1);
		map.put(772.366436467, 1);
		map.put(772.36580217, 3);
		map.put(772.365801467, 2);
		map.put(772.3658011699999, 4);
		map.put(772.3658004670001, 1);
		map.put(772.3624314670001, 1);
		map.put(772.3624301699999, 1);
		map.put(772.36242217, 1);
		map.put(772.3584081700001, 1);
		map.put(772.35840817, 1);
		map.put(772.3584071700001, 1);
		map.put(772.3584071699999, 1);
		map.put(772.354568467, 1);
		map.put(772.354567467, 1);
		map.put(772.35119617, 1);
		map.put(772.351178467, 1);
		map.put(772.34803517, 1);
		map.put(772.34803417, 1);
		map.put(772.347175467, 1);
		map.put(772.3471754669999, 1);
		map.put(772.3471744670001, 4);
		map.put(772.347174467, 1);
		map.put(772.38356017, 2);
		map.put(772.383560467, 1);
		map.put(772.38756517, 1);
		map.put(772.38757317, 1);
		map.put(772.3889204669999, 1);
		map.put(772.3909534669999, 1);
		map.put(772.390953467, 1);
		map.put(772.39229117, 1);
		map.put(772.39479117, 1);
		map.put(772.39479217, 2);
		map.put(772.3947931699998, 1);
		map.put(772.39479317, 7);
		map.put(772.3947931700001, 1);
		map.put(772.3947941700001, 3);
		map.put(772.39479517, 1);
		map.put(772.39879817, 1);
		map.put(772.3988064669999, 1);
		map.put(772.3988071699999, 2);
		map.put(772.39881617, 1);
		map.put(772.3988164670001, 1);
		map.put(772.402186467, 2);
		map.put(772.4021874669999, 1);
		map.put(772.402187467, 1);
		map.put(772.4021874670001, 1);
		map.put(772.402188467, 1);
		map.put(772.4028211699999, 1);
		map.put(772.40602617, 2);
		map.put(772.4062011699999, 1);
		map.put(772.4062094669999, 1);
		map.put(772.406209467, 1);
		map.put(772.408710467, 1);
		map.put(772.4087111700001, 1);
		map.put(772.408711467, 1);
		map.put(772.4087114670001, 1);
		map.put(772.408712467, 1);
		map.put(772.41004817, 2);
		map.put(772.4134211700001, 1);
		map.put(772.41994517, 6);
		map.put(772.4199454669999, 1);
		map.put(772.419945467, 1);
		map.put(772.4199461700001, 2);
		map.put(772.42081417, 1);
		map.put(852.4461604669999, 3);
		map.put(852.4461601700001, 1);
		map.put(852.44616017, 1);
		map.put(852.446159467, 2);
		map.put(852.4461594669999, 1);
		map.put(852.44615917, 2);
		map.put(852.44615817, 1);
		map.put(852.4436584669999, 1);
		map.put(852.439636467, 2);
		map.put(852.4396364669999, 1);
		map.put(852.439635467, 4);
		map.put(852.4396354669999, 1);
		map.put(852.43963517, 2);
		map.put(852.4396344669999, 1);
		map.put(852.43962717, 1);
		map.put(852.436265467, 1);
		map.put(852.436264467, 1);
		map.put(852.4362634669999, 1);
		map.put(852.436255467, 1);
		map.put(852.43625517, 1);
		map.put(852.432424467, 2);
		map.put(852.432407467, 2);
		map.put(852.432240467, 1);
		map.put(852.4290444669998, 1);
		map.put(852.428402467, 1);
		map.put(852.4284021699999, 1);
		map.put(852.428401467, 1);
		map.put(852.425030467, 1);
		map.put(852.425022467, 1);
		map.put(852.425021467, 1);
		map.put(852.4250214669999, 1);
		map.put(852.425013467, 2);
		map.put(852.4250134669999, 1);
		map.put(852.4210094670001, 1);
		map.put(852.421009467, 2);
		map.put(852.42100917, 1);
		map.put(852.4210084670001, 1);
		map.put(852.42100817, 1);
		map.put(852.421007467, 3);
		map.put(852.421006467, 2);
		map.put(852.4209991700001, 1);
		map.put(852.42099917, 1);
		map.put(852.415135467, 1);
		map.put(852.414484467, 1);
		map.put(852.41448417, 1);
		map.put(852.413797467, 1);
		map.put(852.4137964669999, 1);
		map.put(852.413788467, 2);
		map.put(852.411112467, 1);
		map.put(852.409775467, 2);
		map.put(852.4097754669999, 1);
		map.put(852.409774467, 3);
		map.put(852.4097744669999, 1);
		map.put(852.40977417, 1);
		map.put(852.4097741699999, 2);
		map.put(852.4097734669999, 1);
		map.put(852.409767467, 1);
		map.put(852.40976617, 1);
		map.put(852.403893467, 1);
		map.put(852.446160467, 4);
		map.put(852.4461604670001, 2);
		map.put(852.4474984670001, 1);
		map.put(852.450165467, 1);
		map.put(852.4501734669999, 1);
		map.put(852.450173467, 2);
		map.put(852.450174467, 1);
		map.put(852.4515204669999, 1);
		map.put(852.45738317, 1);
		map.put(852.4573841700001, 1);
		map.put(852.457393467, 2);
		map.put(852.4573941699999, 1);
		map.put(852.45739417, 1);
		map.put(852.457394467, 1);
		map.put(852.4573944670001, 2);
		map.put(852.45739517, 1);
		map.put(852.4614074670001, 1);
		map.put(852.461416467, 2);
		map.put(852.4614171700001, 1);
		map.put(852.4647861699999, 2);
		map.put(852.4647874670001, 1);
		map.put(852.4654214669999, 1);
		map.put(852.4654304669999, 1);
		map.put(852.4686271699999, 2);
		map.put(852.4726494669999, 1);
		map.put(852.472650467, 2);
		map.put(852.4825444669999, 2);
		map.put(852.482544467, 8);
		map.put(852.4825454669999, 2);
		map.put(852.482545467, 7);
		map.put(852.4825464669999, 1);
		map.put(852.482546467, 1);
		map.put(852.4825464670001, 2);
		map.put(852.4865504669999, 1);
		map.put(852.4865514669999, 1);
		map.put(885.467623467, 2);
		map.put(885.46762317, 2);
		map.put(885.46110017, 1);
		map.put(885.4610991700001, 1);
		map.put(885.46109917, 4);
		map.put(885.4610991699998, 1);
		map.put(885.45772917, 1);
		map.put(885.4577281700001, 1);
		map.put(885.45772817, 1);
		map.put(885.4577281699999, 1);
		map.put(885.45772717, 2);
		map.put(885.4577271699999, 1);
		map.put(885.4577264669999, 1);
		map.put(885.45771917, 1);
		map.put(885.457259467, 1);
		map.put(885.45387117, 2);
		map.put(885.45370517, 1);
		map.put(885.4537051699999, 1);
		map.put(885.4505164669999, 1);
		map.put(885.4505081699999, 2);
		map.put(885.449866467, 1);
		map.put(885.44986617, 1);
		map.put(885.4498661699998, 1);
		map.put(885.449865467, 1);
		map.put(885.44986517, 5);
		map.put(885.4464941699999, 1);
		map.put(885.44648617, 2);
		map.put(885.4424731700002, 1);
		map.put(885.4424721700001, 1);
		map.put(885.44247217, 3);
		map.put(885.4386314669999, 1);
		map.put(885.4359481700001, 1);
		map.put(885.43594617, 1);
		map.put(885.4352604669999, 1);
		map.put(885.4352601699999, 3);
		map.put(885.43525217, 1);
		map.put(885.4352521699999, 1);
		map.put(885.4352431699999, 1);
		map.put(885.4321071699999, 1);
		map.put(885.43209917, 1);
		map.put(885.43209817, 2);
		map.put(885.4312394670001, 1);
		map.put(885.4312391700001, 1);
		map.put(885.43123817, 2);
		map.put(885.4312374669998, 1);
		map.put(885.425365467, 1);
		map.put(885.42471417, 3);
		map.put(885.42471317, 1);
		map.put(885.4247121699999, 2);
		map.put(885.46762417, 1);
		map.put(885.467624467, 1);
		map.put(885.4676244670001, 1);
		map.put(885.4689621699999, 1);
		map.put(885.47162917, 1);
		map.put(885.4716371699999, 3);
		map.put(885.4716374669999, 1);
		map.put(885.4729751699999, 1);
		map.put(885.4763551699999, 1);
		map.put(885.4788561699999, 1);
		map.put(885.47885617, 2);
		map.put(885.4788571699999, 1);
		map.put(885.47885717, 2);
		map.put(885.4788571700001, 1);
		map.put(885.47885817, 3);
		map.put(885.4788581700001, 2);
		map.put(885.4828631700001, 1);
		map.put(885.48287117, 3);
		map.put(885.4828794669999, 1);
		map.put(885.482879467, 1);
		map.put(885.48288017, 1);
		map.put(885.486252467, 1);
		map.put(885.4900901699999, 1);
		map.put(885.49009017, 1);
		map.put(885.4900911699999, 1);
		map.put(885.4927751700001, 1);
		map.put(885.49411217, 1);
		map.put(885.49411317, 1);
		map.put(885.49411417, 3);
		map.put(885.4974841699999, 1);
		map.put(885.4974851700001, 2);
		map.put(885.50400817, 1);
		map.put(885.50400917, 4);
		map.put(885.5040101700001, 2);
		map.put(885.50487817, 1);
		map.put(885.5053471699999, 1);
		map.put(885.50801517, 1);
		map.put(885.508032467, 1);
		map.put(980.5411234669999, 2);
		map.put(980.541122467, 2);
		map.put(980.5411224669999, 1);
		map.put(980.538621467, 4);
		map.put(980.5386214669999, 1);
		map.put(980.5345994670001, 1);
		map.put(980.534599467, 2);
		map.put(980.5345994669999, 1);
		map.put(980.53459917, 1);
		map.put(980.534598467, 6);
		map.put(980.5345984669999, 1);
		map.put(980.5345974669999, 1);
		map.put(980.531228467, 1);
		map.put(980.531227467, 5);
		map.put(980.5312264669999, 2);
		map.put(980.531218467, 1);
		map.put(980.5312184669999, 1);
		map.put(980.527387467, 2);
		map.put(980.527370467, 2);
		map.put(980.52720517, 1);
		map.put(980.527204467, 5);
		map.put(980.52720417, 2);
		map.put(980.527203467, 1);
		map.put(980.5272034669999, 2);
		map.put(980.5240074669998, 1);
		map.put(980.523365467, 1);
		map.put(980.523364467, 2);
		map.put(980.5199944670001, 1);
		map.put(980.5199934670001, 1);
		map.put(980.5199864670002, 1);
		map.put(980.5199864669999, 1);
		map.put(980.519985467, 3);
		map.put(980.519984467, 1);
		map.put(980.5199844669999, 2);
		map.put(980.519976467, 1);
		map.put(980.5199764669999, 1);
		map.put(980.5174914669999, 1);
		map.put(980.515972467, 2);
		map.put(980.5159721699999, 1);
		map.put(980.5159714670001, 4);
		map.put(980.5159714669999, 1);
		map.put(980.515970467, 9);
		map.put(980.515969467, 2);
		map.put(980.5159694669999, 1);
		map.put(980.510098467, 1);
		map.put(980.509447467, 4);
		map.put(980.509445467, 2);
		map.put(980.508760467, 1);
		map.put(980.5087594669999, 1);
		map.put(980.508751467, 2);
		map.put(980.506075467, 1);
		map.put(980.506074467, 1);
		map.put(980.5055974670001, 1);
		map.put(980.504738467, 2);
		map.put(980.5047384669999, 2);
		map.put(980.504737467, 5);
		map.put(980.5047374669999, 1);
		map.put(980.50473717, 1);
		map.put(980.5047364669999, 2);
		map.put(980.5047304670001, 1);
		map.put(980.50472917, 1);
		map.put(980.5047291699999, 1);
		map.put(980.5007121699999, 1);
		map.put(980.498864467, 2);
		map.put(980.498856467, 1);
		map.put(980.4988554670001, 1);
		map.put(980.4982134670001, 1);
		map.put(980.498213467, 5);
		map.put(980.498212467, 2);
		map.put(980.4982124669999, 1);
		map.put(980.498211467, 1);
		map.put(980.4982114669999, 1);
		map.put(980.4948424669999, 1);
		map.put(980.4948414670001, 1);
		map.put(980.494841467, 2);
		map.put(980.4948414669999, 2);
		map.put(980.4948414669998, 1);
		map.put(980.4948404670001, 1);
		map.put(980.494833467, 3);
		map.put(980.494825467, 1);
		map.put(980.49482517, 1);
		map.put(980.494364467, 1);
		map.put(980.541123467, 4);
		map.put(980.5411234670001, 2);
		map.put(980.541124467, 1);
		map.put(980.5424594670001, 1);
		map.put(980.542460467, 2);
		map.put(980.5424614669998, 1);
		map.put(980.5424614670001, 1);
		map.put(980.545128467, 1);
		map.put(980.545136467, 2);
		map.put(980.545137467, 1);
		map.put(980.5458314670002, 1);
		map.put(980.5464834669999, 1);
		map.put(980.5464834670001, 1);
		map.put(980.5523554669999, 1);
		map.put(980.5523554670001, 1);
		map.put(980.55235617, 1);
		map.put(980.552356467, 5);
		map.put(980.5523564670002, 1);
		map.put(980.5523574669999, 2);
		map.put(980.552357467, 1);
		map.put(980.5523574670001, 5);
		map.put(980.5523584670001, 1);
		map.put(980.5563624669999, 1);
		map.put(980.556369467, 2);
		map.put(980.5563701699999, 1);
		map.put(980.5563704669999, 1);
		map.put(980.5563794669999, 1);
		map.put(980.556379467, 2);
		map.put(980.5597504670001, 1);
		map.put(980.56358917, 1);
		map.put(980.5635894669999, 1);
		map.put(980.563590467, 1);
		map.put(980.5635914669999, 1);
		map.put(980.5676124669999, 1);
		map.put(980.567613467, 5);
		map.put(980.5775074669999, 1);
		map.put(980.577507467, 8);
		map.put(980.5775084669999, 2);
		map.put(980.577508467, 5);
		map.put(980.5775094669999, 1);
		map.put(980.577509467, 1);
		map.put(980.5775094670001, 1);
		map.put(980.5815144669999, 1);
		map.put(980.58152217, 1);
		map.put(980.58153117, 1);
		map.put(980.582217467, 1);
		map.put(980.588740467, 1);
		map.put(980.5887414669999, 2);
		map.put(980.588741467, 4);
		map.put(980.5887414670001, 3);
		map.put(980.58874217, 1);
		map.put(980.588742467, 11);
		map.put(980.5887424670001, 2);
		map.put(998.5516881699999, 4);
		map.put(998.551687467, 1);
		map.put(998.55168717, 3);
		map.put(998.5491861700002, 1);
		map.put(998.5491861699999, 4);
		map.put(998.54516417, 4);
		map.put(998.54516317, 7);
		map.put(998.5451631699999, 1);
		map.put(998.5451621699999, 1);
		map.put(998.54179317, 1);
		map.put(998.5417924669999, 1);
		map.put(998.5417921700001, 2);
		map.put(998.5417921699999, 3);
		map.put(998.54179117, 1);
		map.put(998.5417911699999, 1);
		map.put(998.54178317, 1);
		map.put(998.5417831699999, 1);
		map.put(998.5379521699999, 2);
		map.put(998.53793517, 2);
		map.put(998.5377701700002, 1);
		map.put(998.53777017, 2);
		map.put(998.53776917, 4);
		map.put(998.5377691699999, 3);
		map.put(998.53776817, 1);
		map.put(998.5377681699999, 2);
		map.put(998.5345721699999, 1);
		map.put(998.533930467, 1);
		map.put(998.53393017, 1);
		map.put(998.53392917, 1);
		map.put(998.5305594669999, 1);
		map.put(998.5305584670002, 1);
		map.put(998.530558467, 1);
		map.put(998.5305584669999, 1);
		map.put(998.5305581700001, 1);
		map.put(998.5305511700001, 1);
		map.put(998.53055017, 3);
		map.put(998.5305491700001, 1);
		map.put(998.53054917, 1);
		map.put(998.5305491699999, 1);
		map.put(998.53054117, 2);
		map.put(998.52805617, 1);
		map.put(998.5265371700002, 1);
		map.put(998.5265371700001, 1);
		map.put(998.5265361700001, 2);
		map.put(998.52653617, 3);
		map.put(998.52653517, 6);
		map.put(998.5265351699999, 3);
		map.put(998.52653417, 1);
		map.put(998.5265341699999, 2);
		map.put(998.5206631699999, 1);
		map.put(998.5200121700001, 3);
		map.put(998.52001217, 1);
		map.put(998.5200101700001, 1);
		map.put(998.52001017, 1);
		map.put(998.5193251699999, 1);
		map.put(998.5193241699999, 1);
		map.put(998.5193161699999, 2);
		map.put(998.51664017, 1);
		map.put(998.5166391699998, 1);
		map.put(998.5161621699998, 1);
		map.put(998.5153031700002, 1);
		map.put(998.51530317, 2);
		map.put(998.5153024670001, 1);
		map.put(998.51530217, 3);
		map.put(998.5153021699999, 2);
		map.put(998.51530117, 1);
		map.put(998.5152951700001, 1);
		map.put(998.509429467, 1);
		map.put(998.50942917, 1);
		map.put(998.5094211700001, 1);
		map.put(998.50942017, 1);
		map.put(998.5094201699999, 1);
		map.put(998.50877817, 5);
		map.put(998.5087781699999, 1);
		map.put(998.50877717, 2);
		map.put(998.5087771699999, 1);
		map.put(998.5087761699999, 2);
		map.put(998.5054071700001, 1);
		map.put(998.50540617, 2);
		map.put(998.5054061699999, 4);
		map.put(998.5054051699999, 1);
		map.put(998.50539817, 1);
		map.put(998.5053981699999, 2);
		map.put(998.50539017, 1);
		map.put(998.50492917, 1);
		map.put(998.55168817, 5);
		map.put(998.551688467, 2);
		map.put(998.55302517, 2);
		map.put(998.55302617, 1);
		map.put(998.5530261700001, 1);
		map.put(998.55569317, 1);
		map.put(998.5557011699999, 2);
		map.put(998.55570117, 1);
		map.put(998.55570217, 1);
		map.put(998.5570481699998, 1);
		map.put(998.55704817, 1);
		map.put(998.56292017, 2);
		map.put(998.5629211699999, 1);
		map.put(998.56292117, 2);
		map.put(998.5629211700001, 4);
		map.put(998.56292217, 4);
		map.put(998.5629221700001, 3);
		map.put(998.5629221700002, 1);
		map.put(998.56692717, 1);
		map.put(998.56693417, 2);
		map.put(998.56693517, 1);
		map.put(998.5669441699999, 2);
		map.put(998.566944467, 1);
		map.put(998.5703151700001, 1);
		map.put(998.57415417, 1);
		map.put(998.57415517, 1);
		map.put(998.57415617, 1);
		map.put(998.574337467, 1);
		map.put(998.57817717, 1);
		map.put(998.5781781699999, 1);
		map.put(998.57817817, 2);
		map.put(998.5781781700001, 2);
		map.put(998.58557117, 1);
		map.put(998.5880721699999, 8);
		map.put(998.58807217, 1);
		map.put(998.5880731699999, 3);
		map.put(998.58807317, 4);
		map.put(998.5880741699999, 1);
		map.put(998.5880741700001, 2);
		map.put(998.58941017, 1);
		map.put(998.59207917, 1);
		map.put(998.59278217, 1);
		map.put(998.59930517, 1);
		map.put(998.59930617, 6);
		map.put(998.5993061700001, 4);
		map.put(998.59930717, 8);
		map.put(998.5993071700001, 5);

		double[] keys=map.keys();
		Arrays.sort(keys);

		String peptide="ILQEGVDPK";
		PecanOneFragmentationModel model=new PecanOneFragmentationModel(new FastaPeptideEntry(peptide), PARAMETERS.getAAConstants());
		PecanLibraryEntry entry=model.getPecanSpectrum((byte)2, keys, map, new Range(0f, 200000f), PARAMETERS, false);
		return entry;
	}
}
