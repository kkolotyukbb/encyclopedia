package edu.washington.gs.maccoss.encyclopedia.datastructures;

public interface HasCharge {

	byte getCharge();

}