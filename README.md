EncyclopeDIA

### Contribution guidelines ###
Any contribution must follow the coding style of the project, be presented with tests and stand up to code review before it will be accepted.

### Who do I talk to? ###
This is a [MacCoss Lab](https://sites.google.com/a/uw.edu/maccoss/) project from the University of Washington, [Department of Genome Sciences](http://www.gs.washington.edu/). For more information please contact [Brian Searle](https://isbscience.org/bio/brian-searle/) (bsearle at systemsbiology dot org).